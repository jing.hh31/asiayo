# 環境架設
```
利用Docker架設Nginx, PHP, CentOS環境
Step 1: 進入 docker-compose.yaml 相應的位置，下 「docker-compose up --build -d」
Step 2: 建立vendor相關套件，下「docker exec -it asiayo_php composer update」 
    （若已建立，直接跳Step 3）
Step 3: 在瀏覽器開啟 http://localhost:82/ 成功進入API document頁面，即可使用頁面呼叫API
```
### Docker 指令解釋
- 清快取：docker exec -it asiayo_php php bin/console cache:clear -e prod && chmod 777 app/logs/* && chmod 777 app/cache/*
- 跑測試：docker exec -it asiayo_php ./vendor/bin/phpunit --coverage-html=phpunit
- 建立vendor相關套件：docker exec -it asiayo_php composer update

# 設計模式
- 設計理念

```
以PHP撰寫後端Restful API，框架使用Symfony，遵守Coding Standard使程式乾淨整齊
利用Docker架設lnmp環境(此範例無使用mysql)，使開發過程若遇到版本升級或是其他不可控因素，導致環境有問題
可利用容器重建，重新架構新的容器
```

- 程式

```
＊產生Rate模組，將匯率資料設定於此，並在Controller中呼叫，以此取得相關匯率資料
＊撰寫Functional Tests 驗證整個function是否正確，Controller Unit Tests測試例外訊息是否正確
```