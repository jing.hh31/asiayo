<?php

namespace App\Tests;

use App\Component\JsonResponse;
use Liip\FunctionalTestBundle\Test\WebTestCase as BaseWebTestCase;

class WebTestCase extends BaseWebTestCase
{
    /**
     * 因應PHPUnit 6移除 setExpectedException
     *
     * @param string $exceptionName 預期的例外類型
     * @param string $exceptionMessage 預期的例外訊息
     * @param integer $exceptionCode 預期的例外代碼
     */
    public function setExpectedException($exceptionName, $exceptionMessage = null, $exceptionCode = null)
    {
        $this->expectException($exceptionName);
        if ($exceptionMessage) {
            $this->expectExceptionMessage($exceptionMessage);
        }

        if ($exceptionCode) {
            $this->expectExceptionCode($exceptionCode);
        }
    }

    /**
     * 斷言正常返回
     *
     * @param array|JsonResponse $result
     * @param string $expectedParam
     * @return mixed
     */
    protected function assertOk($result, $expectedParam = 'ret')
    {
        $message = 'Incorrect OK Response';

        if ($result instanceof JsonResponse) {
            $result = json_decode($result->getContent(), true);
        }

        $this->assertIsArray($result);
        $this->assertArrayHasKey('result', $result, $message);
        $this->assertArrayHasKey($expectedParam, $result, $message);
        $this->assertEquals('ok', $result['result'], $message);

        return $result[$expectedParam];
    }
}