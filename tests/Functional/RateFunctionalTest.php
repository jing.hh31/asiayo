<?php

namespace App\Tests\Functional;

use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class RateFunctionalTest extends WebTestCase
{
    public function testGetAction()
    {
        $params = [
            'source' => 'TWD',
            'target' => 'JPY',
        ];

        $expect = [
            'result' => 'ok',
            'ret' => [
                'rate' => '3.67',
            ],
        ];

        $client = self::createClient();
        $client->request(Request::METHOD_GET, '/api/rate', $params);

        self::assertJson($client->getResponse()->getContent());

        $result = json_decode($client->getResponse()->getContent(), true);

        $this->assertOk($result);
        $this->assertEquals($expect, $result);
    }
}
