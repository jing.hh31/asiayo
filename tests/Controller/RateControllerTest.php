<?php

namespace App\Tests\Controller;

use App\Controller\RateController;
use App\Service\Rate;
use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class RateControllerTest extends WebTestCase
{
    /**
     * @var RateController
     */
    private $controller;

    public function setUp(): void
    {
        parent::setUp();
        
        $rate = new Rate();

        $this->controller = new RateController($rate);
    }

    public function testRateWithInvalidSource()
    {
        $this->setExpectedException(
            'InvalidArgumentException',
            'Invalid source/target',
            10001
        );

        $params = [
            'source' => 'kwr',
            'target' => 'JPY',
        ];

        $request = new Request($params, []);
        $this->controller->getAction($request);
    }
}