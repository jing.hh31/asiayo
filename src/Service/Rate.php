<?php

namespace App\Service;

class Rate
{
    const Currency = ['TWD', 'JPY', 'USD'];

    private $rate = '{
  "currencies": {
    "TWD": {
      "TWD": 1,
      "JPY": 3.669,
      "USD": 0.03281
    },
    "JPY": {
      "TWD": 0.26956,
      "JPY": 1,
      "USD": 0.00885
    },
    "USD": {
      "TWD": 30.444,
      "JPY": 111.801,
      "USD": 1
    }
  }
}';

    /**
     * @return array
     */
    public function toArray(): array
    {

        $rate = json_decode($this->rate, true);

        return $rate['currencies'];
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return $this->rate;
    }
}
