<?php

namespace App\Component;

use Symfony\Component\HttpFoundation\JsonResponse as BaseJsonResponse;

class JsonResponse extends BaseJsonResponse
{
    /**
     * 回傳成功JsonResponse
     *
     * @param array|null $data
     * @param array ...$args
     *
     * @return BaseJsonResponse
     */
    public static function ok(array $data = null, array ...$args)
    {
        $output = [
            'result' => 'ok',
            'ret' => $data,
        ];

        foreach ($args as $arg) {
            foreach ($arg as $key => $value) {
                $output[$key] = $value;
            }
        }

        return BaseJsonResponse::create($output);
    }
}
