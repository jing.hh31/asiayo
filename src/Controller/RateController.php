<?php

namespace App\Controller;

use App\Component\JsonResponse;
use App\Service\Rate;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RateController extends AbstractController
{
    private $rates;

    public function __construct(Rate $rate)
    {
        $this->rates = $rate;
    }

    /**
     * @Route("/rate",
     *     name="get_rate",
     *     defaults = {"format" = "json"},
     *     methods = {"GET"}
     * )
     *
     * @OA\Get(
     *     summary="取得匯率",
     *     tags={"Rate"},
     *     @OA\Parameter(name="source", in="query", description="來源幣別(TWD,JPY,USD)", required=true, @OA\Schema(type="string")),
     *     @OA\Parameter(name="target", in="query", description="目標幣別(TWD,JPY,USD)", required=true, @OA\Schema(type="string"))
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        $query = $request->query;
        $source = strtoupper(trim($query->get('source')));
        $target = strtoupper(trim($query->get('target')));

        $check = array_filter([$source, $target]);

        $rates = $this->rates->toArray();

        if (!$source || !$target || array_diff($check, Rate::Currency)) {
            throw new \InvalidArgumentException('Invalid source/target', 10001);
        }

        $rate = round($rates[$source][$target], 2);
        $rate = number_format($rate, 2, '.', ',');

        return JsonResponse::ok(['rate' => $rate]);
    }
}
